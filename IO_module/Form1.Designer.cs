﻿namespace IO_module
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Settings_GroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.scan_coms = new System.Windows.Forms.Button();
            this.cs_enable = new System.Windows.Forms.CheckBox();
            this.coms_view = new System.Windows.Forms.ComboBox();
            this.check_connect = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.scan_the_number_of_devices = new System.Windows.Forms.Button();
            this.Adresses_box = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.textbox_input = new System.Windows.Forms.TextBox();
            this.checkboxlist_input = new System.Windows.Forms.CheckedListBox();
            this.refresh_button = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.set = new System.Windows.Forms.Button();
            this.clear_all = new System.Windows.Forms.Button();
            this.checkboxlist_output = new System.Windows.Forms.CheckedListBox();
            this.textbox_output = new System.Windows.Forms.TextBox();
            this.set_all = new System.Windows.Forms.Button();
            this.Controls_GroupBox = new System.Windows.Forms.GroupBox();
            this.log_richTextBox = new System.Windows.Forms.RichTextBox();
            this.log_groupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.test_button = new System.Windows.Forms.Button();
            this.test_checkBox = new System.Windows.Forms.CheckBox();
            this.Settings_GroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.Controls_GroupBox.SuspendLayout();
            this.log_groupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // Settings_GroupBox
            // 
            this.Settings_GroupBox.Controls.Add(this.tableLayoutPanel2);
            this.Settings_GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Settings_GroupBox.Location = new System.Drawing.Point(3, 3);
            this.Settings_GroupBox.Name = "Settings_GroupBox";
            this.Settings_GroupBox.Size = new System.Drawing.Size(1220, 49);
            this.Settings_GroupBox.TabIndex = 0;
            this.Settings_GroupBox.TabStop = false;
            this.Settings_GroupBox.Text = "Settings";
            this.Settings_GroupBox.Enter += new System.EventHandler(this.Settings_GroupBox_Enter);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.scan_coms, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cs_enable, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.coms_view, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.check_connect, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1214, 30);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // scan_coms
            // 
            this.scan_coms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scan_coms.Location = new System.Drawing.Point(3, 3);
            this.scan_coms.Name = "scan_coms";
            this.scan_coms.Size = new System.Drawing.Size(297, 24);
            this.scan_coms.TabIndex = 0;
            this.scan_coms.Text = "Scan COMs";
            this.scan_coms.UseVisualStyleBackColor = true;
            this.scan_coms.Click += new System.EventHandler(this.scan_coms_Click);
            // 
            // cs_enable
            // 
            this.cs_enable.AutoSize = true;
            this.cs_enable.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cs_enable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cs_enable.Enabled = false;
            this.cs_enable.Location = new System.Drawing.Point(912, 3);
            this.cs_enable.Name = "cs_enable";
            this.cs_enable.Size = new System.Drawing.Size(299, 24);
            this.cs_enable.TabIndex = 1;
            this.cs_enable.Text = "Use check sum";
            this.cs_enable.UseVisualStyleBackColor = true;
            this.cs_enable.CheckedChanged += new System.EventHandler(this.cs_enable_CheckedChanged);
            // 
            // coms_view
            // 
            this.coms_view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.coms_view.FormattingEnabled = true;
            this.coms_view.Location = new System.Drawing.Point(306, 3);
            this.coms_view.Name = "coms_view";
            this.coms_view.Size = new System.Drawing.Size(297, 21);
            this.coms_view.TabIndex = 2;
            this.coms_view.SelectedIndexChanged += new System.EventHandler(this.coms_view_SelectedIndexChanged);
            // 
            // check_connect
            // 
            this.check_connect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.check_connect.Enabled = false;
            this.check_connect.Location = new System.Drawing.Point(609, 3);
            this.check_connect.Name = "check_connect";
            this.check_connect.Size = new System.Drawing.Size(297, 24);
            this.check_connect.TabIndex = 3;
            this.check_connect.Text = "Connect";
            this.check_connect.UseVisualStyleBackColor = true;
            this.check_connect.Click += new System.EventHandler(this.check_connect_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 680F));
            this.tableLayoutPanel3.Controls.Add(this.scan_the_number_of_devices, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Adresses_box, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 124F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1214, 279);
            this.tableLayoutPanel3.TabIndex = 0;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint_1);
            // 
            // scan_the_number_of_devices
            // 
            this.scan_the_number_of_devices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scan_the_number_of_devices.Enabled = false;
            this.scan_the_number_of_devices.Location = new System.Drawing.Point(3, 3);
            this.scan_the_number_of_devices.Name = "scan_the_number_of_devices";
            this.scan_the_number_of_devices.Size = new System.Drawing.Size(528, 24);
            this.scan_the_number_of_devices.TabIndex = 0;
            this.scan_the_number_of_devices.Text = "Scan modules";
            this.scan_the_number_of_devices.UseVisualStyleBackColor = true;
            this.scan_the_number_of_devices.Click += new System.EventHandler(this.scan_the_number_of_devices_Click);
            // 
            // Adresses_box
            // 
            this.Adresses_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Adresses_box.FormattingEnabled = true;
            this.Adresses_box.Location = new System.Drawing.Point(537, 3);
            this.Adresses_box.Name = "Adresses_box";
            this.Adresses_box.Size = new System.Drawing.Size(674, 21);
            this.Adresses_box.TabIndex = 1;
            this.Adresses_box.SelectedIndexChanged += new System.EventHandler(this.Adresses_box_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.groupBox2, 2);
            this.groupBox2.Controls.Add(this.tableLayoutPanel5);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 157);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1208, 119);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Input";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.textbox_input, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.checkboxlist_input, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.refresh_button, 0, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1202, 100);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // textbox_input
            // 
            this.textbox_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textbox_input.Location = new System.Drawing.Point(3, 43);
            this.textbox_input.MaxLength = 4;
            this.textbox_input.Name = "textbox_input";
            this.textbox_input.ReadOnly = true;
            this.textbox_input.Size = new System.Drawing.Size(1196, 20);
            this.textbox_input.TabIndex = 2;
            this.textbox_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textbox_input.TextChanged += new System.EventHandler(this.textbox_input_TextChanged);
            // 
            // checkboxlist_input
            // 
            this.checkboxlist_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkboxlist_input.Enabled = false;
            this.checkboxlist_input.FormattingEnabled = true;
            this.checkboxlist_input.Items.AddRange(new object[] {
            "IN 1",
            "IN 2",
            "IN 3",
            "IN 4",
            "IN 5",
            "IN 6",
            "IN 7",
            "IN 8",
            "IN 9",
            "IN 10",
            "IN 11",
            "IN 12",
            "IN 13",
            "IN 14",
            "IN 15",
            "IN 16"});
            this.checkboxlist_input.Location = new System.Drawing.Point(3, 3);
            this.checkboxlist_input.MultiColumn = true;
            this.checkboxlist_input.Name = "checkboxlist_input";
            this.checkboxlist_input.Size = new System.Drawing.Size(1196, 34);
            this.checkboxlist_input.TabIndex = 2;
            this.checkboxlist_input.SelectedIndexChanged += new System.EventHandler(this.checkboxlist_input_SelectedIndexChanged_1);
            // 
            // refresh_button
            // 
            this.refresh_button.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.refresh_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.refresh_button.Enabled = false;
            this.refresh_button.Location = new System.Drawing.Point(3, 68);
            this.refresh_button.Name = "refresh_button";
            this.refresh_button.Size = new System.Drawing.Size(1196, 29);
            this.refresh_button.TabIndex = 3;
            this.refresh_button.Text = "Refresh";
            this.refresh_button.UseVisualStyleBackColor = true;
            this.refresh_button.Click += new System.EventHandler(this.refresh_button_Click);
            // 
            // groupBox1
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.groupBox1, 2);
            this.groupBox1.Controls.Add(this.tableLayoutPanel4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1208, 118);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Output";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.set, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.clear_all, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.checkboxlist_output, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.textbox_output, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.set_all, 0, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1202, 99);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // set
            // 
            this.set.Dock = System.Windows.Forms.DockStyle.Fill;
            this.set.Enabled = false;
            this.set.Location = new System.Drawing.Point(353, 68);
            this.set.Name = "set";
            this.set.Size = new System.Drawing.Size(344, 28);
            this.set.TabIndex = 1;
            this.set.Text = "SET";
            this.set.UseVisualStyleBackColor = true;
            this.set.Click += new System.EventHandler(this.set_Click);
            // 
            // clear_all
            // 
            this.clear_all.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clear_all.Enabled = false;
            this.clear_all.Location = new System.Drawing.Point(703, 68);
            this.clear_all.Name = "clear_all";
            this.clear_all.Size = new System.Drawing.Size(496, 28);
            this.clear_all.TabIndex = 2;
            this.clear_all.Text = "Clear All";
            this.clear_all.UseVisualStyleBackColor = true;
            this.clear_all.Click += new System.EventHandler(this.clear_all_Click);
            // 
            // checkboxlist_output
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.checkboxlist_output, 3);
            this.checkboxlist_output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkboxlist_output.Enabled = false;
            this.checkboxlist_output.FormattingEnabled = true;
            this.checkboxlist_output.Items.AddRange(new object[] {
            "OUT 1",
            "OUT 2",
            "OUT 3",
            "OUT 4",
            "OUT 5",
            "OUT 6",
            "OUT 7",
            "OUT 8",
            "OUT 9",
            "OUT 10",
            "OUT 11",
            "OUT 12",
            "OUT 13",
            "OUT 14",
            "OUT 15",
            "OUT 16"});
            this.checkboxlist_output.Location = new System.Drawing.Point(3, 3);
            this.checkboxlist_output.MultiColumn = true;
            this.checkboxlist_output.Name = "checkboxlist_output";
            this.checkboxlist_output.Size = new System.Drawing.Size(1196, 34);
            this.checkboxlist_output.TabIndex = 1;
            this.checkboxlist_output.MouseClick += new System.Windows.Forms.MouseEventHandler(this.checkboxlist_output_MouseClick);
            this.checkboxlist_output.Leave += new System.EventHandler(this.checkboxlist_output_Leave);
            // 
            // textbox_output
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.textbox_output, 3);
            this.textbox_output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textbox_output.Enabled = false;
            this.textbox_output.Location = new System.Drawing.Point(3, 43);
            this.textbox_output.MaxLength = 4;
            this.textbox_output.Name = "textbox_output";
            this.textbox_output.Size = new System.Drawing.Size(1196, 20);
            this.textbox_output.TabIndex = 2;
            this.textbox_output.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textbox_output.TextChanged += new System.EventHandler(this.textbox_output_TextChanged);
            this.textbox_output.Leave += new System.EventHandler(this.textbox_output_Leave);
            // 
            // set_all
            // 
            this.set_all.Dock = System.Windows.Forms.DockStyle.Fill;
            this.set_all.Enabled = false;
            this.set_all.Location = new System.Drawing.Point(3, 68);
            this.set_all.Name = "set_all";
            this.set_all.Size = new System.Drawing.Size(344, 28);
            this.set_all.TabIndex = 3;
            this.set_all.Text = "Set All";
            this.set_all.UseVisualStyleBackColor = true;
            this.set_all.Click += new System.EventHandler(this.set_all_Click);
            // 
            // Controls_GroupBox
            // 
            this.Controls_GroupBox.Controls.Add(this.tableLayoutPanel3);
            this.Controls_GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Controls_GroupBox.Location = new System.Drawing.Point(3, 58);
            this.Controls_GroupBox.Name = "Controls_GroupBox";
            this.Controls_GroupBox.Size = new System.Drawing.Size(1220, 298);
            this.Controls_GroupBox.TabIndex = 1;
            this.Controls_GroupBox.TabStop = false;
            this.Controls_GroupBox.Text = "Controls";
            this.Controls_GroupBox.Enter += new System.EventHandler(this.Controls_GroupBox_Enter);
            // 
            // log_richTextBox
            // 
            this.log_richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.log_richTextBox.Location = new System.Drawing.Point(3, 16);
            this.log_richTextBox.Name = "log_richTextBox";
            this.log_richTextBox.Size = new System.Drawing.Size(1214, 169);
            this.log_richTextBox.TabIndex = 0;
            this.log_richTextBox.Text = "";
            this.log_richTextBox.TextChanged += new System.EventHandler(this.log_richTextBox_TextChanged);
            // 
            // log_groupBox
            // 
            this.log_groupBox.Controls.Add(this.log_richTextBox);
            this.log_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.log_groupBox.Location = new System.Drawing.Point(3, 417);
            this.log_groupBox.Name = "log_groupBox";
            this.log_groupBox.Size = new System.Drawing.Size(1220, 188);
            this.log_groupBox.TabIndex = 2;
            this.log_groupBox.TabStop = false;
            this.log_groupBox.Text = "Log";
            this.log_groupBox.Enter += new System.EventHandler(this.log_groupBox_Enter);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.Controls_GroupBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Settings_GroupBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.log_groupBox, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 304F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1226, 608);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel6);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 362);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1220, 49);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Test";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.test_button, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.test_checkBox, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1214, 30);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // test_button
            // 
            this.test_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.test_button.Location = new System.Drawing.Point(3, 3);
            this.test_button.Name = "test_button";
            this.test_button.Size = new System.Drawing.Size(601, 24);
            this.test_button.TabIndex = 0;
            this.test_button.Text = "Test";
            this.test_button.UseVisualStyleBackColor = true;
            this.test_button.Click += new System.EventHandler(this.test_button_Click);
            // 
            // test_checkBox
            // 
            this.test_checkBox.AutoSize = true;
            this.test_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.test_checkBox.Enabled = false;
            this.test_checkBox.Location = new System.Drawing.Point(610, 3);
            this.test_checkBox.Name = "test_checkBox";
            this.test_checkBox.Size = new System.Drawing.Size(601, 24);
            this.test_checkBox.TabIndex = 1;
            this.test_checkBox.Text = "End on error";
            this.test_checkBox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1226, 608);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "IO Module";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Settings_GroupBox.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.Controls_GroupBox.ResumeLayout(false);
            this.log_groupBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox Settings_GroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button scan_coms;
        private System.Windows.Forms.ComboBox coms_view;
        private System.Windows.Forms.CheckBox cs_enable;
        private System.Windows.Forms.Button check_connect;
        private System.Windows.Forms.GroupBox Controls_GroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button scan_the_number_of_devices;
        private System.Windows.Forms.ComboBox Adresses_box;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox textbox_input;
        private System.Windows.Forms.CheckedListBox checkboxlist_input;
        private System.Windows.Forms.Button refresh_button;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button set;
        private System.Windows.Forms.Button clear_all;
        private System.Windows.Forms.CheckedListBox checkboxlist_output;
        private System.Windows.Forms.TextBox textbox_output;
        private System.Windows.Forms.Button set_all;
        private System.Windows.Forms.GroupBox log_groupBox;
        private System.Windows.Forms.RichTextBox log_richTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button test_button;
        private System.Windows.Forms.CheckBox test_checkBox;
    }
}

