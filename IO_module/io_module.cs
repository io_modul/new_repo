﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;



namespace IO_module
{
    class io_module
    {
        private SerialPort serialPort;
        private Crc16 crc;

        public enum command
        {
            W,
            R,
            RW
        }

        public enum address
        {
            DDEVICE_0 = 0x01,
            DEVICE_1 = 0x02,
            DEVICE_2 = 0x04,
            DEVICE_3 = 0x08,
            DEVICE_4 = 0x10,
            DEVICE_5 = 0x20,
            DEVICE_6 = 0x40,
            DEVICE_7 = 0x80
        }

        public enum answear
        {
            NW_FORMAT = 0x00,
            NW_CS,
            NW_E,
            AW,
            NR_FORMAT,
            NR_CS,
            NR_E,
            AR
        }

        //public variables
        public const int baud = 9600;
        public const Parity parity = Parity.None;
        public const int data_bits = 8;
        public const StopBits stop_bits = StopBits.One;
        public const Handshake handshake = Handshake.None;
        public const string new_line = "\r";
        public bool cs_enable = false;

        public UInt16[] outputs = new UInt16[8];

        public io_module(string com_port, bool CS)
        {
            //crc init
            crc = new Crc16();
            cs_enable = CS;

            //serial port init
            serialPort = new SerialPort();
            serialPort.PortName = com_port;
            serialPort.BaudRate = 115200;
            serialPort.Parity = Parity.None;
            serialPort.DataBits = data_bits;
            serialPort.StopBits = stop_bits;
            serialPort.Handshake = handshake;
            serialPort.NewLine = "\r";

            // Set the read/write timeouts
            serialPort.ReadTimeout = 5000;
            serialPort.WriteTimeout = 5000;
        }

        public void Connect()
        {
            if (!serialPort.IsOpen)
                serialPort.Open();

            //code for getting device status
        }

        public void Disconnect()
        {
            if (serialPort.IsOpen)
                serialPort.Close();
        }

        public byte ScanDevices()
        {
            byte device_mask = 0;
            UInt16[] tmp_data = new UInt16[8];
            if (SendCommand(io_module.command.R, 0xFF, tmp_data, cs_enable) == io_module.answear.NR_E)
            {
                device_mask = (byte)tmp_data[0];
            }
            else
                device_mask = 0xFF;

            return device_mask;
        }

        public void WriteData(byte address, UInt16 data)
        {
            UInt16[] tmp_data = { data };
            if (SendCommand(io_module.command.W, (byte)(1 << (address - 1)), tmp_data, cs_enable) != io_module.answear.AW)
                throw new Exception("Writting error. Wrong answear received!");
        }

        public UInt16 ReadData(byte address)
        {
            UInt16[] tmp_data = { 0 };
            if (SendCommand(io_module.command.R, (byte)(1 << (address - 1)), tmp_data, cs_enable) != io_module.answear.AR)
                throw new Exception("Reading error. Wrong answear received!");
            return tmp_data[0];
        }

        public UInt16 ReadWrite(byte address, UInt16 output_data)
        {
            UInt16[] read_data = { 0 };
            UInt16[] write_data = { output_data };
            if (ReadWriteCommand((byte)(1 << (address - 1)), (byte)(1 << (address - 1)), write_data, read_data, cs_enable) != io_module.answear.AR)
                throw new Exception("Reading error. Wrong answear received!");
            return read_data[0];
        }

        //second address mask is used only for combined read write command (read command address)
        //if combined read write command is used the read answears are returned

        private answear ReadWriteCommand(byte readAddressMask, byte writeAddressMask, UInt16[] writeData, UInt16[] readData, bool useCS)
        {
            int numberOfReadDevices = 0;
            int numberOfWriteDevices = 0;
            string message = "";
            string answear = "";
            string CS = "XX";

            //get number of devices
            for (int i = 0; i < 8; i++)
            {
                numberOfReadDevices += (readAddressMask >> i) & 0x01;
                numberOfWriteDevices += (writeAddressMask >> i) & 0x01;
            }

            //len of data buffer check
            if (readData.Length < numberOfReadDevices || writeData.Length < numberOfWriteDevices)
                throw new Exception("Data buffer size error. Buffer size should be equal to number of addresed devices.");

            //create data string
            string data_string = "";
            for (int i = 0; i < numberOfWriteDevices; i++)
            {
                if (i == 0)
                    data_string = String.Format("{0:x4}", writeData[i]);
                else
                    data_string = String.Format("{0} {1:x4}", data_string, writeData[i]);
            }

            //send message and check answear
            if (useCS)
            {
                message = String.Format("{0} {1:X2} {2} {3:X4} {4}", command.R.ToString(), readAddressMask, command.W.ToString(), writeAddressMask, data_string);
                byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message);
                UInt16 cs = crc.ComputeChecksum(message_bytes);
                message = String.Format("{0}{1:X4}", message, cs);
            }
            else
            {
                message = String.Format("{0} {1:X2} {2} {3:X2} {4} {5}{6}", command.R.ToString(), readAddressMask, command.W.ToString(), writeAddressMask, data_string, CS, CS);
            }

            serialPort.WriteLine(message);
            answear = serialPort.ReadLine();
            serialPort.ReadByte(); //read 0A byte
            if (!String.Equals(answear, message, StringComparison.Ordinal)) //check if answear equals to message
                throw new Exception("Received answear is not equal to message");
            answear = serialPort.ReadLine();// read of read command answear
            serialPort.ReadByte(); //read 0A byte
            serialPort.ReadLine(); //read W answear 
            serialPort.ReadByte(); //read 0A byte

            //split answear
            char[] separators = { ' ', ',' };
            string[] separated_answear = answear.Split(separators);

            //if answear is AR
            if (separated_answear[0].Equals("AR", StringComparison.Ordinal))
            {
                //crc check
                if (useCS)
                {
                    string crc_string = answear.Split(' ').Last();
                    string message_string = answear.Remove(answear.Length - 4);
                    UInt16 crc_value = Convert.ToUInt16(crc_string, 16);
                    byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message_string);
                    if (crc_value != crc.ComputeChecksum(message_bytes))
                        throw new Exception("Checksum error");
                }

                //copy received data
                for (int i = 2; i < separated_answear.Length - 1; i++)
                {
                    readData[i - 2] = Convert.ToUInt16(separated_answear[i], 16);
                }
                return io_module.answear.AR;
            }

            //else error received
            else
            {
                //copy received data
                for (int i = 0; i < separated_answear.Length - 3; i++)
                {
                    readData[i] = Convert.ToUInt16(separated_answear[i + 2], 16);
                }
                switch (String.Format("{0} {1}", separated_answear[0], separated_answear[1]))
                {
                    case "NR FORMAT":
                        {

                            return io_module.answear.NR_FORMAT;
                        }
                    case "NR CS":
                        {
                            return io_module.answear.NR_CS;
                        }
                    case "NR E":
                        {
                            return io_module.answear.NR_E;
                        }
                    default:
                        {
                            throw new Exception("Bad answear received!");
                        }
                }
            }
        }

        public answear SendCommand(command command, byte addressMask, UInt16[] data, bool useCS)
        {
            string CS = "XX";
            string message = "";
            string answear = "";
            int numberOfDevices = 0;
            UInt16[] tmp_data = data;

            //get number of devices
            for (int i = 0; i < 8; i++)
                numberOfDevices += (addressMask >> i) & 0x01;

            //len of data buffer check
            if (data.Length < numberOfDevices)
                throw new Exception("Data buffer size error. Buffer size should be equal to number of addresed devices.");

            //create data string
            string data_string = "";
            for (int i = 0; i < numberOfDevices; i++)
            {
                if (i == 0)
                    data_string = String.Format("{0:x4}", data[i]);
                else
                    data_string = String.Format("{0} {1:x4}", data_string, data[i]);
            }

            //send and receive command
            switch (command)
            {
                case command.R:
                    {
                        if(useCS)
                        {
                            message = String.Format("{0} {1:X2} ", command.ToString(), addressMask);
                            byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message);
                            UInt16 cs = crc.ComputeChecksum(message_bytes);
                            message = String.Format("{0}{1:X4}", message, cs);
                        }
                        else
                        {
                            message = String.Format("{0} {1:X2} {2}{3}", command.ToString(), addressMask, CS, CS);
                        }
                        serialPort.WriteLine(message);
                        answear = serialPort.ReadLine();
                        serialPort.ReadByte(); //read 0A byte
                        if (!String.Equals(answear, message, StringComparison.Ordinal)) //check if answear equals to message
                            throw new Exception("Received answear is not equal to answear");
                        answear = serialPort.ReadLine();
                        serialPort.ReadByte(); //read 0A byte
                        break;
                    }
                case command.W:
                    {

                        //send message and check answear
                        if (useCS)
                        {
                            message = String.Format("{0} {1:X2} {2} ", command.ToString(), addressMask, data_string);
                            byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message);
                            UInt16 cs = crc.ComputeChecksum(message_bytes);
                            message = String.Format("{0}{1:X4}", message, cs);
                        }
                        else
                        {
                            message = String.Format("{0} {1:X2} {2} {3}{4}", command.ToString(), addressMask, data_string, CS, CS);
                        }
                        serialPort.WriteLine(message);
                        answear = serialPort.ReadLine();
                        serialPort.ReadByte(); //read 0A byte
                        if (!String.Equals(answear, message, StringComparison.Ordinal)) //check if answear equals to message
                            throw new Exception("Received answear is not equal to message");
                        answear = serialPort.ReadLine();
                        serialPort.ReadByte(); //read 0A byte
                        break;
                    }
            }

            //split answear
            char[] separators = { ' ', ',' };
            string[] separated_answear = answear.Split(separators);

            //if answear is AW
            if (separated_answear[0].Equals("AW", StringComparison.Ordinal))
            {
                data = tmp_data;
                return io_module.answear.AW;
            }

            //if answear is AR
            else if (separated_answear[0].Equals("AR", StringComparison.Ordinal))
            {
                //crc check
                if (useCS)
                {
                    string crc_string = answear.Split(' ').Last();
                    string message_string = answear.Remove(answear.Length - 4);
                    UInt16 crc_value = Convert.ToUInt16(crc_string, 16);
                    byte[] message_bytes = System.Text.Encoding.ASCII.GetBytes(message_string);
                    if (crc_value != crc.ComputeChecksum(message_bytes))
                        throw new Exception("Checksum error");
                }

                //copy received data
                for (int i = 2; i < separated_answear.Length - 1; i++)
                {
                    tmp_data[i - 2] = Convert.ToUInt16(separated_answear[i], 16);
                }
                data = tmp_data;
                return io_module.answear.AR;
            }

            //else error received
            else
            {
                //copy received data
                for (int i = 0; i < separated_answear.Length - 3; i++)
                {
                    tmp_data[i] = Convert.ToUInt16(separated_answear[i + 2], 16);
                }
                data = tmp_data;
                switch (String.Format("{0} {1}", separated_answear[0], separated_answear[1]))
                {
                    case "NW FORMAT":
                        {
                            return io_module.answear.NW_FORMAT;
                        }
                    case "NW CS":
                        {
                            return io_module.answear.NW_CS;
                        }
                    case "NW E":
                        {
                            return io_module.answear.NW_E;
                        }
                    case "NR FORMAT":
                        {

                            return io_module.answear.NR_FORMAT;
                        }
                    case "NR CS":
                        {
                            return io_module.answear.NR_CS;
                        }
                    case "NR E":
                        {
                            return io_module.answear.NR_E;
                        }
                    default:
                        {
                            throw new Exception("Bad answear received!");
                        }
                }
            }
        }
    }
}
