﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace IO_module
{
    
    public partial class Form1 : Form
    {
        private io_module io_mod;
        private UInt16[] outputs = new UInt16[8] { 0, 0, 0, 0, 0, 0, 0, 0};

        public Form1()
        {
            InitializeComponent();
        }


        private void scan_coms_Click(object sender, EventArgs e)
        {
            try
            {
                //scan avalible coms
                string[] Ports = SerialPort.GetPortNames();
                coms_view.Items.Clear();
                //fill scanned com ports into combo box
                for (int i = 0; i < Ports.Length; i++)
                {
                    coms_view.Items.Add(Ports[i]);
                }

                //set combo box selected index on first
                if (Ports.Length > 0)
                coms_view.SelectedIndex = 0;

                //write message into log
                log_richTextBox.SelectionColor = Color.Green;
                log_richTextBox.AppendText(String.Format("[ ! ] Scanning done\n"));
            }
            catch(Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText(String.Format("[ ! ] Scanning error: {0}\n", ex.Message));
            }
        }

        private void coms_view_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (coms_view.SelectedIndex > -1) { check_connect.Enabled = true; };
        }

        private void cs_enable_CheckedChanged(object sender, EventArgs e)
        {
            io_mod.cs_enable = cs_enable.Checked;
        }

        private void check_connect_Click(object sender, EventArgs e)
        {
            try
            {
                //create IO module instance from com in combo box 
                io_mod = new io_module(coms_view.SelectedItem.ToString(), false);
                //connect 
                io_mod.Connect();
                scan_the_number_of_devices.Enabled = true;
                log_richTextBox.SelectionColor = Color.Green;
                log_richTextBox.AppendText("[ ! ] Connected to " + coms_view.SelectedItem.ToString() + "\n");

                //enable buttons
                scan_the_number_of_devices.Enabled = true;
            }
            catch (Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText("[ ! ] Connecting error: " + ex.Message);
            }

        }

        private void scan_the_number_of_devices_Click(object sender, EventArgs e)
        {
            try
            {
                //scan devices bitmask
                byte mask = io_mod.ScanDevices();

                //if there is no device 
                if(mask == 0xFFFF)
                {
                    log_richTextBox.SelectionColor = Color.Green;
                    log_richTextBox.AppendText("[ ! ] IO modules scanning done, but modules not founded\n");
                }

                //if some device is connected
                else
                {
                    //clear combobox with addreses
                    Adresses_box.Items.Clear();

                    //check for avalible devices in scanned mask
                    for (int i = 0; i < sizeof(byte) * 8; i++)
                    {

                        if (((mask >> i) & 1) == 0)
                            Adresses_box.Items.Add(i + 1);
                    }

                    Adresses_box.SelectedIndex = 0;

                    //read inputs
                    byte address = byte.Parse(Adresses_box.Text);           
                    UInt16 received_data = io_mod.ReadData(address);

                    //setup input checkbox and textbox
                    for (int i = 0; i < checkboxlist_input.Items.Count; i++)
                        checkboxlist_input.SetItemChecked(i, ((received_data >> i) & 1) == 1);
                    textbox_input.Text = received_data.ToString();

                    //setup outputs
                    for (int i = 0; i < checkboxlist_output.Items.Count; i++)
                        checkboxlist_output.SetItemChecked(i, ((outputs[address] >> i) & 1) == 1);
                    textbox_output.Text = outputs[address].ToString();

                    //enable buttons
                    set.Enabled = true;
                    clear_all.Enabled = true;
                    set_all.Enabled = true;
                    textbox_output.Enabled = true;
                    checkboxlist_output.Enabled = true;
                    refresh_button.Enabled = true;
                    cs_enable.Enabled = true;
                    test_checkBox.Enabled = true;
                    log_richTextBox.SelectionColor = Color.Green;
                    log_richTextBox.AppendText("[ ! ] IO modules scanning done\n");
                }
            }
            catch(Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText("[ ! ] IO modules scanning error: " + ex.Message + "\n");
            }
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Settings_GroupBox_Enter(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void Adresses_box_SelectedIndexChanged(object sender, EventArgs e)
        {
            //read inputs
            byte address = byte.Parse(Adresses_box.Text);
            UInt16 received_data = io_mod.ReadData(address);

            //setup output checkbox
            for (int i = 0; i < checkboxlist_output.Items.Count; i++)
            {
                checkboxlist_output.SetItemChecked(i, ((outputs[address - 1] >> i) & 1) == 1);
            }

            //setup  output textbox
            textbox_output.Text = String.Format("{0:x4}", (outputs[address - 1]));
        }

        private void checkboxlist_input_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
        }

        private void log_groupBox_Enter(object sender, EventArgs e)
        {
            
        }

        private void log_richTextBox_TextChanged(object sender, EventArgs e)
        {
            log_richTextBox.ScrollToCaret();
        }

        private void textbox_output_Leave(object sender, EventArgs e)
        {
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void textbox_input_TextChanged(object sender, EventArgs e)
        {

        }

        private void set_all_Click(object sender, EventArgs e)
        {
            byte address = byte.Parse(Adresses_box.Text);
            try
            {
                io_mod.WriteData(address, 0xFFFF);

                //setup output checkbox
                for (int i = 0; i < checkboxlist_output.Items.Count; i++)
                {
                    checkboxlist_output.SetItemChecked(i, true);
                }

                //setup  output textbox
                textbox_output.Text = String.Format("{0:x4}", 0xFFFF);

                log_richTextBox.SelectionColor = Color.Green;
                log_richTextBox.AppendText(String.Format("[ ! ] Setting outputs of {0} done\n", address));

                //setup ouput variable
                outputs[address - 1] = 0xFFFF;
            }
            catch (Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText(String.Format("[ ! ] Setting outputs of {0} error: {1}\n", address, ex.Message));

                //enable buttons
                set.Enabled = false;
                clear_all.Enabled = false;
                set_all.Enabled = false;
                textbox_output.Enabled = false;
                checkboxlist_output.Enabled = false;
                refresh_button.Enabled = false;
                cs_enable.Enabled = false;
                test_checkBox.Enabled = false;
            }
        }

        private void clear_all_Click(object sender, EventArgs e)
        {
            byte address = byte.Parse(Adresses_box.Text);
            try
            {
                io_mod.WriteData(address, 0x0000);

                //setup output checkbox
                for (int i = 0; i < checkboxlist_output.Items.Count; i++)
                {
                    checkboxlist_output.SetItemChecked(i, false);
                }

                //setup  output textbox
                textbox_output.Text = String.Format("{0:x4}", 0x0000);

                //setup ouput variable
                outputs[address - 1] = 0x0000;

                log_richTextBox.SelectionColor = Color.Green;
                log_richTextBox.AppendText(String.Format("[ ! ] Clearing outputs of {0} done\n", address));
            }
            catch (Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText(String.Format("[ ! ] Clearing outputs of {0} error: {1}\n", address, ex.Message));

                //enable buttons
                set.Enabled = false;
                clear_all.Enabled = false;
                set_all.Enabled = false;
                textbox_output.Enabled = false;
                checkboxlist_output.Enabled = false;
                refresh_button.Enabled = false;
                cs_enable.Enabled = false;
                test_checkBox.Enabled = false;
            }
        }

        private void set_Click(object sender, EventArgs e)
        {
            //get address
            byte address = byte.Parse(Adresses_box.Text);
            UInt16 data = Convert.ToUInt16(textbox_output.Text, 16);
            try
            {
                //write data
                io_mod.WriteData(address, data);
                log_richTextBox.SelectionColor = Color.Green;
                log_richTextBox.AppendText(String.Format("[ ! ] Setting value {0:x4} on outputs of {1} done\n", textbox_output.Text, address));
                outputs[address - 1] = data;
            } 
            catch (Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText(String.Format("[ ! ] Setting value {0} on outputs of {1} error: {2}\n", textbox_output.Text,  address, ex.Message));

                //enable buttons
                set.Enabled = false;
                clear_all.Enabled = false;
                set_all.Enabled = false;
                textbox_output.Enabled = false;
                checkboxlist_output.Enabled = false;
                refresh_button.Enabled = false;
                cs_enable.Enabled = false;
                test_checkBox.Enabled = false;
            }
        }

        private void refresh_button_Click(object sender, EventArgs e)
        {
            //read inputs
            byte address = byte.Parse(Adresses_box.Text);

            try
            {
                UInt16 received_data = io_mod.ReadData(address);

                //setup input checkbox and textbox
                for (int i = 0; i < checkboxlist_input.Items.Count; i++)
                    checkboxlist_input.SetItemChecked(i, ((received_data >> i) & 1) == 1);
                textbox_input.Text = received_data.ToString();
                address = byte.Parse(Adresses_box.Text);
                log_richTextBox.SelectionColor = Color.Green;
                log_richTextBox.AppendText(String.Format("[ ! ] Refresh input value of {0} done: {1:x4}\n",  address, received_data));

            }
            catch (Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText(String.Format("[ ! ] Reading input value of {1} error: {2}\n", textbox_output.Text, address, ex.Message));

                //enable buttons
                set.Enabled = false;
                clear_all.Enabled = false;
                set_all.Enabled = false;
                textbox_output.Enabled = false;
                checkboxlist_output.Enabled = false;
                refresh_button.Enabled = false;
                cs_enable.Enabled = false;
                test_checkBox.Enabled = false;
            }
        }

        private void checkboxlist_output_MouseClick(object sender, MouseEventArgs e)
        {
            UInt16 data = 0;
            for (int i = 0; i < checkboxlist_output.Items.Count; i++)
            {
                if (checkboxlist_output.GetItemChecked(i))
                    data |= (UInt16)(1 << i);
            }
        }

        private void textbox_output_TextChanged(object sender, EventArgs e)
        {
            try
            {
                UInt16 data = Convert.ToUInt16(textbox_output.Text, 16);
                //setup input checkbox and textbox
                for (int i = 0; i < checkboxlist_output.Items.Count; i++)
                    checkboxlist_output.SetItemChecked(i, ((data >> i) & 1) == 1);
            }
            catch (Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText(String.Format("[ ! ] Bad format of data {0:x2} error: {1}\n", textbox_output.Text, ex.Message));

            }
        }

        private void checkboxlist_output_Leave(object sender, EventArgs e)
        {
            UInt16 data = 0;
            //get value from checkbox and display it in texbox
            for (int i = 0; i < checkboxlist_output.Items.Count; i++)
            {
                if (checkboxlist_output.GetItemChecked(i))
                    data |= (UInt16)(1 << i);
            }
            textbox_output.Text = String.Format("{0:x4}", data);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {

        }

        private void Controls_GroupBox_Enter(object sender, EventArgs e)
        {

        }

        private void test_button_Click(object sender, EventArgs e)
        {
            byte address = byte.Parse(Adresses_box.Text);
            UInt16 received_data = 0;
            int error_count = 0;
            int input_output = 0;
            try
            {
                for (int i = 0; i < 0xFFFF; i++)
                {
                    received_data = io_mod.ReadWrite(address, (UInt16)i);
                    if (received_data != i)
                    {
                        if (test_checkBox.Checked)
                        {
                            input_output = i;
                            break;
                        }
                        error_count++;
                    }
                }
                log_richTextBox.SelectionColor = Color.Green;
                if (test_checkBox.Checked)
                    log_richTextBox.AppendText(String.Format("[ ! ] Testing done with error on INPUT/OUTPUT {0}\n", input_output));
                else
                    log_richTextBox.AppendText(String.Format("[ ! ] Testing done with {0} errors\n", error_count));                            
            }
            catch (Exception ex)
            {
                log_richTextBox.SelectionColor = Color.Red;
                log_richTextBox.AppendText(String.Format("[ ! ] Testing error {0}\n", ex.Message));

            }
        }
    }
}
